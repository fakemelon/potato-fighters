﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skins : MonoBehaviour
{
    public Skin[] skins;
    public Button buyButton;
    public Image preview;
    public Text diamondsText;
    private int currentSkin;
    private int diamonds;

    private void Start()
    {
        for (int i = 0; i < skins.Length; i++)
        {
            if (!PlayerPrefs.HasKey("skin" + i))
            {
                PlayerPrefs.SetInt("skin", 0);
            }
        }
        if (!PlayerPrefs.HasKey("skin"))
        {
            PlayerPrefs.SetInt("skin", 0);
        }
        PlayerPrefs.SetInt("skin0", 1);
        diamonds = PlayerPrefs.HasKey("diamonds") ? PlayerPrefs.GetInt("diamonds") : 0;
        diamondsText.text = diamonds.ToString();
        PlayerPrefs.Save();
        UpdateSkin();
    }

    public void NextSkin()
    {
        currentSkin = currentSkin == skins.Length - 1 ? 0 : currentSkin + 1;
        UpdateSkin();
    }

    public void LastSkin()
    {
        currentSkin = currentSkin == 0 ? skins.Length - 1 : currentSkin - 1;
        UpdateSkin();
    }

    public void Buy()
    {
        if (PlayerPrefs.GetInt("skin" + currentSkin) == 1)
        {
            PlayerPrefs.SetInt("skin", currentSkin);
        }
        else
        {
            PlayerPrefs.SetInt("skin" + currentSkin, 1);
            diamonds -= skins[currentSkin].price;
            PlayerPrefs.SetInt("diamonds", diamonds);
            diamondsText.text = diamonds.ToString();
        }
        PlayerPrefs.Save();
        UpdateSkin();
    }

    private void UpdateSkin()
    {
        Text buttonText = buyButton.GetComponentInChildren<Text>();
        buttonText.text = PlayerPrefs.GetInt("skin" + currentSkin) == 1 ? "Equip" : "Buy";
        int equippedSkin = PlayerPrefs.GetInt("skin");
        if (currentSkin == equippedSkin)
        {
            buttonText.text = "Equipped";
        }
        bool isButtonInteractable = true;
        if (PlayerPrefs.GetInt("skin" + currentSkin) == 1)
        {
            if (currentSkin == equippedSkin)
            {
                isButtonInteractable = false;
            }
        }
        else
        {
            if (skins[currentSkin].price > diamonds)
            {
                isButtonInteractable = false;
            }
        }
        buyButton.interactable = isButtonInteractable;
        preview.sprite = skins[currentSkin].previewImage;
    }
}

[Serializable]
public class Skin
{
    public Sprite previewImage;
    public int price;
    public GameObject prefab;
}
